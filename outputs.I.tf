output "I-git" {
  value = local.git_info
}

output "I-logging_bucket" {
  value = {
    id  = module.tfstate-production.I-logging_bucket_id
    arn = module.tfstate-production.I-logging_bucket_arn
  }
}

output "I-state_bucket" {
  value = {
    id  = module.tfstate-production.I-state_bucket_id
    arn = module.tfstate-production.I-state_bucket_arn
  }
}

output "I-lock_table" {
  value = {
    id  = module.tfstate-production.I-lock_table_id
    arn = module.tfstate-production.I-lock_table_arn
  }
}
