module "tfstate" {
  source            = "github.com/peopleware/terraform-ppwcode-modules//tfstate?ref=v%2F5%2F0%2F0"
  organisation_name = "pink-ribbon-belgium-dev.org"
  region            = "eu-west-1"
  tags = {
    repository  = local.git_info.repo
    branch      = local.git_info.branch
    sha         = local.git_info.sha
    environment = "development"
  }
}

module "tfstate-production" {
  source            = "github.com/peopleware/terraform-ppwcode-modules//tfstate?ref=v%2F5%2F0%2F0"
  organisation_name = "pink-ribbon-belgium.org"
  region            = "eu-west-1"
  tags = {
    repository  = local.git_info.repo
    branch      = local.git_info.branch
    sha         = local.git_info.sha
    environment = "production"
  }
}
