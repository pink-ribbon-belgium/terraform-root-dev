Pink Ribbon Belgium Dev Terraform Root
======================================

Root of **Pink Ribbon Belgium Dev** [Terraform] definition.

**This configuration does not follow the ppwcode conventions completely.**

This configuration is a bootstrap configuration. It defines the S3 bucket and DynamoDB table necessary
for [Terraform] operation. Because this configuration defines the S3 bucket that stores [Terraform] remote state,
and the DynamoDB table that guards against concurrent modification, it does not itself have remote state, nor is
this configuration guarded against concurrent modification. **In this case, the state file should be consistently
committed to the git repository.**

This infrastructure is set up in `eu-west-1` (Ireland). At the time of creation, `eu-north-1` (Stockholm) is a bit
cheaper, but it does not have all required services yet.

The AWS profile used in these configurations is `pink-ribbon-belgium-admin` for users with administrator access,
and `pink-ribbon-belgium-dev` for users with developer access,

The FQDN used to make names unique is `pink-ribbon-belgium.org`.


Structure
---------

This [Terraform] configuration uses the [ppwcode conventions][Terraform] as much as possible.

- [`main.tf`](main.tf) defines the S3 bucket that holds the [Terraform] remote state, and the DynamoDB table that holds
  all locks, for all Pink Ribbon Belgium Dev [Terraform] configurations. 
   
This configuration does not have different environments. There is only the 1 production infrastructure.
Dependent configurations would not be able to use a separate root infrastructure to store their remote state,
since the reference to the remote state cannot be interpolated in [Terraform].



Using the bucket as remote state
--------------------------------

This configuration only defines the infrastructure needed by other, functionally meaningful, [Terraform] configurations.
Those should configure their `remote_state` to be stored in the S3 bucket managed by this module, and to
guard against concurrent modification using the DynamoDB table managed by this module.

A functionally meaningful configuration `<CONFIGURATION_NAME>` does that by including a _backend definition_:

    terraform {
      backend "s3" {
        bucket         = "tfstate.pink-ribbon-belgium.org"
        key            = "<CONFIGURATION_NAME>.tfstate"
        region         = "eu-west-1"
        profile        = "pink-ribbon-belgium-dev"
        encrypt        = true
        dynamodb_table = "tfstate-lock.pink-ribbon-belgium.org"
      }
    }

`<CONFIGURATION_NAME>` _should_ match the name of the configuration, i.e., the name of the git repository it
is defined in.

You can use the outputs of a configuration `<CONFIGURATION_NAME>` configured this way in another configuration
`foo-bar-baz` by including a _remote state data definition_ in `foo-bar-baz`:

    data "terraform_remote_state" "<CONFIGURATION_NAME>" {
      backend = "s3"
      config {
        bucket      = "tfstate.pink-ribbon-belgium.org"
        environment = "${terraform.env}"
        key         = "<CONFIGURATION_NAME>.tfstate"
        region      = "eu-west-1"
      }
    }

You should consider whether the `environment` value is appropriate to your use case, and change it appropriately.

This works only if you also included an _aws provider definition_ that has access to the remote state bucket
and DynamoDB table defined by this module. Most often, it will be the same profile as above:
    
    provider "aws" {
      region  = "eu-west-1"
      profile = "pink-ribbon-belgium-dev"
    }

See [Using S3 as a Terraform backend] and [AWS credentials].



Getting started
---------------

The infrastructure is defined using [Terraform].
See [Getting started with a Terraform configuration].



[Terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[Getting started with a Terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
[AWS credentials]: https://peopleware.atlassian.net/wiki/x/RoAWBg
[Using S3 as a Terraform backend]: https://www.terraform.io/docs/backends/types/s3.html
