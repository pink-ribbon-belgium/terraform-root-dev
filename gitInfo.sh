#!/bin/bash

# Exit if any of the intermediate steps fail
set -e

REPO=`git config --get remote.origin.url`
BRANCH=`git rev-parse --abbrev-ref HEAD`
SHA=`git rev-parse --verify HEAD`

echo "{\"repo\": \"${REPO}\", \"branch\": \"${BRANCH}\", \"sha\": \"${SHA}\"}"
