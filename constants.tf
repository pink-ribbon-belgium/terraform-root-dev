provider "external" { version = "~> 1.2" }

data "external" "git_info" {
  program = ["./gitInfo.sh"]
}

locals {
  profile  = "pink-ribbon-belgium-admin"
  region   = "eu-west-1"
  git_info = data.external.git_info.result
}
